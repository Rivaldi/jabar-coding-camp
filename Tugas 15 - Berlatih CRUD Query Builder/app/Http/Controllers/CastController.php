<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function index () {
       $castDB = DB::table('cast')->get();
    // dd($posts);
        return view('castDB.cast',compact('posts'));
       
    }
    public function create () {
        return view('castDB.create');
    }
    public function store (Request $Request) {
        
        // dd($Request->all());
        $Request->validate([
            'Title' => 'required|max:255',
            'Body' => 'required',
        ]);

        DB::table('cast')->insert([
            'Title' => $Request["Title"],
            'Body' => $Request["Body"]
        ]);
        return redirect('cast')->with('success','Data Berhasil Ditambah');;
    }

    public function show ($id){
        $posts = DB::table('post')->where('id',$id)->first();
        // dd($posts);
        return view ('castDB.show',compact('posts'));
    }
    public function edit ($id){
        $posts = DB::table('cast')->where('id',$id)->first();
        // dd($posts);
        return view ('castDB.edit',compact('posts'));
    }

    public function update ($id,  Request $Request) {
        
        // dd($Request->all());
        $Request->validate([
            'Title' => 'required|max:255',
            'Body' => 'required',
        ]);

        DB::table('cast')
            ->where('id',$id)
            ->update([
            'Title' => $Request["Title"],
            'Body' => $Request["Body"]
        ]);
        return redirect('cast')->with('success','Data Berhasil Diubah');;
    }
    public function destroy ($id) {
        
       
        DB::table('cast')
            ->where('id',$id)
            ->delete();
        return redirect('cast')->with('success','Data Berhasil Dihapus');;
    }
}
