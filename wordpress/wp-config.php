<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=R<M!lt@!$o:+i0p+Vrxx)Jh3<5el|BE`* q)}>t[blQx@X8_E_7*^6WLEgq]%KZ' );
define( 'SECURE_AUTH_KEY',  'I@4* b+qDwwe528!X!lco>aww49Hj5G!&gV*k*_oR68_0ft~l&H-iF0xk`>im<@^' );
define( 'LOGGED_IN_KEY',    'kl{Q{sy9%*lUZQH) @Nl=WtUE:=ms#=YAF4^l?*gov[l(vKm~SW%wzHGaJ%5+HDX' );
define( 'NONCE_KEY',        '6/Y{;*WCxVBYm.uQ.sWwJ;C;&W/o{rRJ`P|Vm;U+@+S*~kR?CWH_Pg<N^6m1D/._' );
define( 'AUTH_SALT',        '|V5X3sjt_X:vVgG1HsILCNvFsc91tQGqih1~`+ P3cxtb720k P`O7o/),0s?k_L' );
define( 'SECURE_AUTH_SALT', '2MDz.LNm0/T`QK%-pN|KW8iY7/:L=>JMvWM<GGt8%*Lf{%wQyK!1oEZv@WXXm/!j' );
define( 'LOGGED_IN_SALT',   '63!gG[Hn$t1kIJ;Vq~oyif:;b ?abE+1k>%X6K,A-0nZ.`nn$ev[vpEaL,i)$Pax' );
define( 'NONCE_SALT',       'wQKbIEg#3K!Nx13@UXJZ$qOoMG`v6(-~*D5:On|>n!&$XWYOn<0(?!}vF.w3uF?;' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
